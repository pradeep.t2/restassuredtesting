package apiTesting;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.*;

public class PostMethod {
	@Test
	public void postOperationsmethod() {
		JSONObject js=new JSONObject();
		js.put("name", "Pradeep");
		js.put("job", "Trainee");
		
		System.out.println(js);
	baseURI = "https://reqres.in/api";
//		given().get("/api/users?page=2").then().statusCode(200);
   given().body(js.toJSONString()).when().post("/users").then().statusCode(201);
		
	}

}
