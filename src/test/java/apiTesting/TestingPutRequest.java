package apiTesting;

import org.testng.annotations.Test;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import io.restassured.response.ValidatableResponse;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class TestingPutRequest {

	@Test
	public void tesingPutRequest() {
		JSONObject js = new JSONObject();
		js.put("name", "Pradeepkt");
		js.put("job", "TestEngineer");
		System.out.println(js);
		given().body(js.toJSONString()).put("https://reqres.in/api/users/2").then().statusCode(200).body("updatedAt",
				greaterThan("2023-01-24T09:59:40.438Z"));
	}

	@Test
	public void TestingPatchRequest() {
		JSONObject js = new JSONObject();
		js.put("name", "Virat");
		js.put("job", "Devloper");
		System.out.println(js);
		given().body(js.toJSONString()).patch("https://reqres.in/api/users/2").then().statusCode(200).body("updatedAt",
				greaterThan("2023-01-24T10:03:33.826Z"));

	}

}
